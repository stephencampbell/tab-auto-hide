import sublime
import sublime_plugin

class do_toggle_tabs(sublime_plugin.TextCommand):
  def run(self, view, args):
    if sublime.Window.get_tabs_visible() == True:
      sublime.Window.set_tabs_visible(False)
    else:
      sublime.Window.set_tabs_visible(True)
      
class toggle_tabs(sublime_plugin.EventListener):
  def toggle_tabs(self):
    for group in range(sublime.active_window().num_groups()):
      if len(sublime.active_window().views_in_group(group)) > 1:
        sublime.active_window().set_tabs_visible(True)
        break
      else:
        sublime.active_window().set_tabs_visible(False)
  def on_close(self, view):
    self.toggle_tabs()
  def on_activated(self, view):
    self.toggle_tabs()