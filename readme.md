## What is this project?
Tab Auto Hide is a really simple Sublime Text 3 extension
that automatically hides the tab bar when only a single
tab is visible.

## How do I use Tab Auto Hide?
Simply stick `tabautohide.py` into the `User` subfolder
of your Sublime Text plugins folder and Tab Auto Hide
should automatically be enabled!